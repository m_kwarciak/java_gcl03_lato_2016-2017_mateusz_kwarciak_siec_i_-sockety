package Controllers;

import CLIENT.ClientThread;
import USERS.HashPassword;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ResourceBundle;

/**
 * Created by Mateusz on 06.05.2017.
 */
public class SignUpController implements Initializable {

    @FXML
    private TextField passwordField;

    @FXML
    private TextField loginField;

    @FXML
    private TextField ageField;

    @FXML
    private TextField addressField;

    @FXML
    private TextField sexField;

    @FXML
    private Label statusLabel;

    @FXML
    void backButtonAction(ActionEvent event) throws IOException {
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("../FXML/SignInScene.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

        app_stage.setScene(home_page_scene);
        app_stage.show();
    }

    @FXML
    void clearButtonAction() {
        loginField.clear();
        passwordField.clear();
        ageField.clear();
        addressField.clear();
        sexField.clear();
        statusLabel.setText("");

    }

    @FXML
    void saveButtonAction() throws InterruptedException, NoSuchAlgorithmException {

        ClientThread clientThread = new ClientThread();
        clientThread.start();
        HashPassword hashPassword = new HashPassword();

        String data;
        data = loginField.getText() + ";" + hashPassword.hash(passwordField.getText()) + ";" + ageField.getText() + ";" + addressField.getText() + ";" + sexField.getText();

        clientThread.signUp("$%RGST%", data);


        clearButtonAction();
        statusLabel.setText("Success");

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
