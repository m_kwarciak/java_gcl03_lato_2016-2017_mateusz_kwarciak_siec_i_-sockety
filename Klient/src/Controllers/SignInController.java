
package Controllers;

import CLIENT.ClientThread;
import USERS.HashPassword;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ResourceBundle;

public class SignInController implements Initializable {

    @FXML
    private Button signInButton;

    @FXML
    private TextField loginField;

    @FXML
    private Button signUpButton;

    @FXML
    private Button exitButton;

    @FXML
    private PasswordField passwordField;

    @FXML
    private Label statusLabel;
    
    @FXML
    public void onAction(ActionEvent event) throws IOException, InterruptedException, NoSuchAlgorithmException {

        ClientThread clientThread = new ClientThread();
        clientThread.start();
        HashPassword hashPassword = new HashPassword();

        boolean status = clientThread.signUp(loginField.getText(), hashPassword.hash(passwordField.getText()));


        if(status)
        {
            Parent home_page_parent = FXMLLoader.load(getClass().getResource("../FXML/ChatScene.fxml"));
            Scene home_page_scene = new Scene(home_page_parent);
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

            app_stage.setScene(home_page_scene);
            app_stage.show();
        }
        else
        {
            statusLabel.setText("DENIAL");
        }



    }


    @FXML
    void exitButtonAction(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    void signUpButtonAction(ActionEvent event) throws IOException {
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("../FXML/SignUpScene.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

        app_stage.setScene(home_page_scene);
        app_stage.show();

    }

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    




}

