package USERS;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Mateusz on 02.05.2017.
 */
public class HashPassword {
    public  String hash(String password) throws NoSuchAlgorithmException {

        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(password.getBytes());

        byte byteData[] = md.digest();

        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            stringBuffer.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }

        return stringBuffer.toString();
    }
}
