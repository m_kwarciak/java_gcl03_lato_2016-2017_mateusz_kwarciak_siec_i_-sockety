package USERS;

import java.util.ArrayList;

/**
 * Created by Mateusz on 26.04.2017.
 */
public class UserList {
    private static ArrayList<String> userLoggedList = new ArrayList<String>();

   public void setUserLoggedList(String string)
   {
       String[] parts = string.split( ";" );
       if (parts.length != 0)
       {
           for (String el: parts)
           {
               if(!el.isEmpty()) {
                   userLoggedList.add(el);
               }
           }
       }
   }

   public void getUserLoggedList()
   {
       System.out.println("Lista zalogowanych uzytkownikow");
       for(String el: userLoggedList)
       {
           System.out.println(el.toString());
       }
   }

   public String whoWrite(String msg)
   {
       String tmp = msg.substring(6,21);
       String[] parts = tmp.split("#");
       if(!parts[0].isEmpty())
       {
           return parts[0];
       }
       return null;
   }

   public void LOGIN(String msg){
       userLoggedList.add(msg.substring(1));
       //System.out.println("UserList.LOGIN dodano:" + msg.substring(1));
   }

   public void LOGOUT(String msg)
   {
       String tmp = msg.substring(1);
       for(String el: userLoggedList)
       {
           if(el.equals(tmp)) {
               userLoggedList.remove(el);
               System.out.print("UserList.LOGOUT usunieto:" + tmp);
           }
       }
   }
}
