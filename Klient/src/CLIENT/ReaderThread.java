package CLIENT;

import USERS.UserList;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

/**
 * Created by Mateusz on 17.04.2017.
 */
public class ReaderThread extends Thread{

    private Socket socket;
    private DataInputStream inputStream;
    private boolean access;

    public ReaderThread(Socket socket, DataInputStream inputStream)
    {
        this.socket = socket;
        this.inputStream = inputStream;
        this.access = true;
    }

    public void run()
    {
        UserList userList = new UserList();
        String msg = "";
        while (access)
        {
            try {

                try{msg=inputStream.readUTF();}catch (EOFException e){}


                int index = MessageType.decode(msg);

                switch (index)
                {
                    case 0:  //ERROR
                    {
                        System.out.println("Lost connection");
                        access = false;
                        return;
                    }
                    case 1:  //REGISTER
                    {
                        System.out.println("REGISTER: " + msg.substring(1));
                        break;
                    }
                    case 2:  //LOGIN
                    {
                       userList.LOGIN(msg);
                       System.out.println("LOGIN: " + msg.substring(1));
                       break;
                    }
                    case 3:  //LOGOUT
                    {
                        userList.LOGOUT(msg);
                        System.out.println("LOGOUT: " + msg.substring(1));
                        break;
                    }
                    case 4:  //MESSAGE
                    {
                       System.out.println("MESSAGE: " + msg.substring(1));
                        break;
                    }
                    case 5:  //STATISTICS
                    {
                        System.out.println("STATISTICS: " + msg.substring(1));
                        break;
                    }case 6:  //EXIT
                    {
                        System.out.println("Lost connection");
                        access = false;
                        return;
                    }default:
                    {
                        break;
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
                access=false;
            }
        }
        return;
    }

    public void cancelReaderThread()
    {
        access=false;
    }
    public boolean status()
    {
        return access;
    }
}
