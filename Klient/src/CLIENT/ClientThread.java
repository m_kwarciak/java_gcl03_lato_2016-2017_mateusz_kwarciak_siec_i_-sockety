package CLIENT;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by Mateusz on 15.04.2017.
 */
public class ClientThread  extends Thread{
    private static String  host = "localhost";
    private String login;
    private String password;
    private boolean loop = true;
    private int connected = 0;
    private ReaderThread readerThread;

    public void run()
    {
        while(loop)
        {

        }

            try {
                Socket socket = new Socket(host, 777);
                System.out.println("Connected to localhost on port 777");

                DataInputStream inputStream = new DataInputStream(socket.getInputStream());
                DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());

                Scanner scanner = new Scanner(System.in);
                String logInMSG, msgToSend;

                outputStream.writeUTF(login);
                outputStream.writeUTF(password);
                logInMSG = inputStream.readUTF();



                if(logInMSG.equals("ACCEPT"))
                {
                    connected =1;
                    System.out.print("Zalogowano:" + login);
                    readerThread = new ReaderThread(socket, inputStream);
                    readerThread.start();
                    while (readerThread.status())
                    {
                        msgToSend=scanner.nextLine();
                        outputStream.writeUTF(msgToSend);
                    }
                    socket.close();
                    return;
                }else if(logInMSG.equals("REGISTER"))
                {
                    return;
                } else {
                    connected=-1;
                    System.out.print("Blad logowania:" + login);

                    inputStream.close();
                    outputStream.close();
                    socket.close();
                }


            } catch (IOException e) {
                //e.printStackTrace();
                System.out.println("ERROR Nie udalo sie polaczyc z serverem");
            }

        return;
    }



    public void disconnect()
    {
        this.readerThread.cancelReaderThread();
    }

    public boolean signUp(String login, String password) throws InterruptedException {
        this.login = login;
        this.password = password;

        loop=false;

        Thread.sleep(500);
        if(connected==1)
        {  return true; }
        else
        {  return false; }

    }

}
