import java.io.IOException;


public class Main {

	public static void main(String[] args) throws InterruptedException, IOException {

		final Logger[] loggers = new Logger[]
				{
						new ConsoleLogger(),
						new CommunicatorLogger()
				};


		Crawler crawler = new Crawler();
		crawler.sciezka="plik.txt";


		crawler.addStudentaddedListener((Student)->loggers[0].log("Dodano",Student));
		crawler.addStudentremovedListener((Student)->loggers[0].log("Usunieto",Student));

		crawler.addStudentremovedListener((Student)->loggers[1].log("Usunieto",Student));
		crawler.addStudentaddedListener((Student)->loggers[1].log("Dodano",Student));

		try {
			crawler.run();
		}
		catch (CrawlerException e)
		{
			e.pisz();
		}





	}

}
