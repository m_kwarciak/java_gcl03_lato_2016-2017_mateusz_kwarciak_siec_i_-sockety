package USERS;

import SERVER.ClientThread;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mateusz on 22.04.2017.
 */
public class UserList {

    public static List<User> userList;
    public static List<User> userLoggedList= new ArrayList<User>();
    private static ArrayList<ClientThread> listThread = new ArrayList<>();
    private final static String path = "users.txt";

    public UserList()
    {
        this.userList = loadUser();
    }

    private List<User> loadUser(){
        File file = new File(path);
        try {
            List<User> list = UserParser.parse(file);
            return list;
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("ERROR - Wczytywanie listy użytkowników z pliku");
        }
        return null;
    }

    public static String userLoggedNicknameList()
    {
      String string = new String();
      for(User el: userLoggedList)
      {
          string +=el.getLogin() +";";
      }
      return string;
    }

    public void addThread(ClientThread clientThread)
    {
        listThread.add(clientThread);
    }

    public void removeThread(ClientThread clientThread)
    {
        listThread.remove(clientThread);
    }

    public ArrayList<ClientThread> getListThread ()
    {
        return listThread;
    }

    public void register(String tmp) throws IOException {
        User user = new User();
        String[] parts = tmp.split(";");

            user.setLogin(parts[0]);
            user.setPassword(parts[1]);
            user.setAge(Integer.parseInt(parts[2]));
            user.setAdress(parts[3]);
            user.setSex(parts[4]);

            userList.add(user);

            Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("users.txt", true)));
            writer.write(tmp);
            writer.write(System.lineSeparator());
            writer.close();

    }

}
