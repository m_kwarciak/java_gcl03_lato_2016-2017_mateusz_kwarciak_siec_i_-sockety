package SERVER;

import USERS.User;
import USERS.UserList;

import java.io.*;
import java.net.Socket;

/**
 * Created by Mateusz on 17.04.2017.
 */
public class ClientThread extends Thread
{
    private Socket socket;
    private User user;
    private static boolean access;

    private DataInputStream inputStream;
    private DataOutputStream outputStream;

    public ClientThread(Socket socket)
    {

        this.socket=socket;
        try {
                inputStream = new DataInputStream(socket.getInputStream());
                outputStream = new DataOutputStream(socket.getOutputStream());

        }catch (IOException e)
        {
            e.printStackTrace();
        }
        start();
    }

    public void run() {
        access = logIn();  ////////Logowanie


        try
        {
        while(access)
        {
            String msg= inputStream.readUTF();
            int index = MessageType.decode(msg);
            switch (index){
                case 0:  //ERROR
                {
                    System.out.println("ERROR: "+ msg);
                    break;
                }
                case 1:  //REGISTER
                {
                    break;
                }
                case 2:  //LOGIN
                {
                    //W LogIn()
                   break;
                }
                case 3:  //LOGOUT
                {
                    System.out.println("LOGOUT: "+user.getLogin());
                    outputStream.writeUTF("3" + user.getLogin());
                    UserList.userLoggedList.remove(user);
                    sendMessage("3"+ user.getLogin());
                    return;
                }
                case 4:  //MESSAGE
                {
                    sendMessage(msg);
                    break;
                }
                case 5:  //STATISTICS
                {

                    break;
                }
                case 6:  //EXIT
                {
                    UserList.userLoggedList.remove(this.user);
                    sendMessage("3"+this.user.getLogin());;
                    inputStream.close();
                    outputStream.close();
                    socket.close();
                    access = false;
                    UserList list = new UserList();
                    list.removeThread(this);
                    System.out.println("EXIT: " + user.getLogin());
                    return ;
                }
                default:
                {
                    break;
                }
            }
        }

        }catch (IOException e)
        {
            //e.printStackTrace();
            UserList.userLoggedList.remove(user);
            //System.out.println("ERROR Nieoczekiwanie zakonczono polaczenie z uzytkownikiem");
            return;
        }
        return;
    }

    private boolean logIn()
    {
        String message;

        try {
            message = inputStream.readUTF();
            if(message.equals("$%RGST%")) {           //REGISTRY
                message = inputStream.readUTF();
                UserList userList = new UserList();
                userList.register(message);
                outputStream.writeUTF("REGISTER");
                return false;
            }
            for (User el : UserList.userList) {       //LOGIN
                if (el.getLogin().equals(message)) {
                    message = inputStream.readUTF();
                    if(el.getPassword().equals(message))
                    {
                        if(el.getLogin().equals("logger"))
                        {
                            el.setSocket(socket);
                            user = el;
                            UserList.userLoggedList.add(user);
                            return true;
                        }else
                        {
                            outputStream.writeUTF("ACCEPT");
                            el.setSocket(socket);
                            user = el;
                            sendMessage("2" + user.getLogin());  //LOGIN
                            UserList.userLoggedList.add(user);
                            System.out.println(el.getLogin() + ": ZALOGOWANO");
                            return true;
                        }
                    }
                    else {
                        outputStream.writeUTF("DENIAL");
                    }
                }
            }

        }catch (IOException e)
        {
            e.printStackTrace();
            try {
                outputStream.writeUTF("Blad logowania");
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            return false;
        }
        try {
            outputStream.writeUTF("Blad logowania");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void sendMessage(String msg)
    {
        Socket tmpSocket;
        DataOutputStream tmpOutputStream;

        for (User el: UserList.userLoggedList)
        {
            if(!el.getLogin().equals("logger"))
            {
                try {
                    tmpSocket = el.getSocket();
                    tmpOutputStream = new DataOutputStream(tmpSocket.getOutputStream());
                    tmpOutputStream.writeUTF(msg);
                    System.out.println("Wyslano wiadomosc do: " + el.getLogin() + " TRESC: " + msg);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    public void exit() throws IOException {
        UserList.userLoggedList.remove(this.user);
        outputStream.writeUTF("6");
        inputStream.close();
        outputStream.close();
        socket.close();
        access = false;
        return ;
    }


}


