package Program;

import SERVER.ClientThread;
import SERVER.Server;
import USERS.UserList;
import javafx.application.Platform;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by Mateusz on 06.05.2017.
 */
public class Main {

    public static void main(String args[]) throws IOException
    {
        Scanner scanner = new Scanner(System.in);
        Server server = null;
        System.out.println("Start - s\nStop - x\nExit - e");
        boolean loop = true;
        boolean status = false;

        while (loop)
        {
            String tmp = scanner.nextLine();
            if(tmp.equals("s"))
            {
                if(!status)
                {
                    System.out.println("SERVER: START");
                    server = new Server();
                    server.start();
                    status = true;
                }
            }else if(tmp.equals("x"))
            {   if(status)
                {
                System.out.println("SERVER: STOP");
                server.STOP();
                status = false;
                }
            }else if (tmp.equals("e"))
            {
                if(status)
                {
                    System.out.println("EXIT");
                    server.STOP();
                    return;
                }else
                {
                    System.out.println("EXIT");
                    return;
                }


            }else
            {
                System.out.print("Error");
            }

        }
        return;
    }
}
